from django.contrib import admin
from django.urls import path, include
from authentication import urls as authentication_urls
from stock.urls import router as stock_urls
from utils.routers import DefaultRouter

router = DefaultRouter()
router.extend(stock_urls)

urlpatterns = [
    path(r'admin/', admin.site.urls),
    path(r'api/token/', include((authentication_urls, 'authentication'), namespace='auth')),
    path(r'api/', include(router.urls)),
]
