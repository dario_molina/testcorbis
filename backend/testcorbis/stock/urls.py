from rest_framework import routers

from stock.views import StockViewSet, CategoryViewSet

router = routers.DefaultRouter()
router.register('stock', StockViewSet)
router.register('category', CategoryViewSet)
