from django.contrib import admin

from stock.models import Product, Category

admin.site.register(Product)
admin.site.register(Category)
