from rest_framework import serializers

from stock.models import Product, Category


class CategorySerializer(serializers.ModelSerializer):
    id = serializers.PrimaryKeyRelatedField(queryset=Category.objects.all(), required=True)

    class Meta:
        model = Category
        fields = ['id', 'name']


class StockSerializer(serializers.ModelSerializer):
    categories = CategorySerializer()

    class Meta:
        model = Product
        fields = ['id', 'code', 'name', 'price', 'quantity', 'categories']

    def create(self, validated_data):
        category = validated_data.pop('categories', None).get('id')
        category = Category.objects.get(id=category.id)
        validated_data['categories'] = category
        product = Product.objects.create(**validated_data)
        return product

    def update(self, instance, validated_data):
        category = validated_data.pop('categories', None).get('id')
        category = Category.objects.get(id=category.id)
        validated_data['categories'] = category
        Product.objects.filter(id=instance.id).update(**validated_data)
        return validated_data
