from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel


class Product(TimeStampedModel):
    code = models.CharField(_('Code'), max_length=25)
    name = models.CharField(_('Name'), max_length=100)
    price = models.PositiveIntegerField(_('Price'), default=0)
    quantity = models.PositiveIntegerField(_('Quantity'), default=0)
    categories = models.ForeignKey(
        'stock.Category', related_name='products', on_delete=models.CASCADE,  verbose_name=_(u'Categories')
    )

    def __str__(self):
        return '{}'.format(self.name)


class Category(TimeStampedModel):
    name = models.CharField(_(u'Name'), max_length=100)

    def __str__(self):
        return '{}'.format(self.name)
