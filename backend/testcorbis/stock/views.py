from rest_framework.viewsets import ModelViewSet

from stock.models import Product, Category
from stock.serializers import StockSerializer, CategorySerializer


class StockViewSet(ModelViewSet):
    serializer_class = StockSerializer
    queryset = Product.objects.all()


class CategoryViewSet(ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
