Django==2.0
djangorestframework==3.9.1
django-filter==2.1.0
Pillow==5.4.1
Markdown==3.0.1
djangorestframework-simplejwt==4.1.0
django-extensions==2.0.5
psycopg2==2.7.6.1
