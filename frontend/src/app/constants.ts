import { environment } from "@env/environment";

export const URL_BACKEND = environment.URL_BACKEND;
export const URL_MEDIA = URL_BACKEND + "/media/";
export const URL_API = URL_BACKEND;