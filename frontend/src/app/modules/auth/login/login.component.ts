import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@app/services/auth/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})

export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  private form = {};
  private loginError = {};

  ngOnInit() {
  }

  public login() {
    this.authService.login(this.form).subscribe(
      (data: any) => {
        sessionStorage.setItem('auth_token', data.access);
        this.router.navigate(['/home']);
      },
      (error: any) => {
        this.loginError = error.error;
      }
    );
  }
}
