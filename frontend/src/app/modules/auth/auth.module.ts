import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { AuthService } from '@app/services/auth/auth.service';
import { LoginComponent } from './login/login.component';
import { AuthRoutesModule } from './auth.routes';

@NgModule({
  providers: [AuthService],
  declarations: [
    LoginComponent,
  ],
  imports: [
    CommonModule,
    AuthRoutesModule,
    FormsModule,
    HttpClientModule,
  ]
})

export class AuthModule { }
