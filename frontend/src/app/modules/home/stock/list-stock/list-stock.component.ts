import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StockService } from '@app/services/stock/stock.service';


@Component({
  selector: 'app-list-stock',
  templateUrl: './list-stock.component.html',
})

export class ListStockComponent implements OnInit {

  constructor(private stockService: StockService, private router: Router) { }

  private dataSource;
  private stockError = {};
  private idToRemove;
  private confirmation = false;
  private filterName = '';

  ngOnInit() {
    this.getAllProducts();
  }

  public deleteProduct() {
    this.stockService.deleteProduct(this.idToRemove).subscribe(
      (res: any) => {
        this.getAllProducts();
        this.idToRemove = null;
        this.confirmation = true;
        this.router.navigate(['/home/list']);
      },
      (error: any) => {
        console.log(error);
      }
    )
  }

  public confirmDeleted(id) {
    this.idToRemove = id;
  }

  public getAllProducts() {
    this.stockService.getAllProducts().subscribe(
      (res: any) => {
        this.dataSource = res;
      },
      (error: any) => {
        this.stockError = error.error;
        console.log(this.stockError);
      }
    );
  }

}