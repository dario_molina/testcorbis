import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { StockService } from '@app/services/stock/stock.service';

@Component({
  selector: 'app-update-stock',
  templateUrl: './update-stock.component.html',
})

export class UpdateStockComponent implements OnInit {

  private categories = [];
  private form = {
    'verbose_name': '',
    'categories': null,
  };
  public productId;
  public stockError = {};

  constructor(
    private stockService: StockService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {
      this.activatedRoute.params.subscribe(params => {
        this.productId = params['productId'];
      });
  }

  ngOnInit() {
    this.retrieveCategories()
    this.stockService.getProduct(this.productId).subscribe(
      (response: any) => {
        this.form = response;
        this.form.categories['verbose_name'] = `${response.categories.id} - ${response.categories.name}`;
      },
      (error: any) => {
        console.log(error);
      }
    );

  }

  public retrieveCategories(querytext?) {
    let queryObj = (querytext ? {'querytext': querytext}: undefined);
    this.stockService.getCategories(queryObj).subscribe((response: any) => {
      this.categories = response.map((item) => {
        item.verbose_name = `${item.id} - ${item.name}`;
        return item;
      });
    });
  }

  public updateProduct() {
    if (this.form) {
      delete this.form.verbose_name;
    };
    this.stockService.updateProduct(this.productId, this.form).subscribe(
      (response: any) => {
        this.router.navigate(['/home/list']);
      },
      (error: any) => {
        this.stockError = error.error;
        console.log(error.error);
      }
    )
  }

}