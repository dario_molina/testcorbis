import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { StockService } from '@app/services/stock/stock.service';

@Component({
  selector: 'app-detail-stock',
  templateUrl: './detail-stock.component.html',
})

export class DetailStockComponent implements OnInit {

  public productId;
  private form = {
    'id': null, 
    'code': '', 
    'name': '', 
    'price': 0, 
    'quantity': 0,
    'categories': null,
  };

  constructor(
    private stockService: StockService, 
    private router: Router,
    private activatedRoute: ActivatedRoute) {
      this.activatedRoute.params.subscribe(params => {
        this.productId = params['productId'];
      });
  }

  ngOnInit() {
    this.stockService.getProduct(this.productId).subscribe(
      (res: any) => {
        this.form = res;
      },
      (error: any) => {
        console.log(error);
      }
    )
  }

}