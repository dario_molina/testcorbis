import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StockService } from '@app/services/stock/stock.service';


@Component({
  selector: 'app-create-stock',
  templateUrl: './create-stock.component.html',
})

export class CreateStockComponent implements OnInit {

  constructor(private stockService: StockService, private router: Router) { }

  private categories = [];
  public form = {
    'categories': null,
  };
  private stockError = {};

  ngOnInit() {
    this.retrieveCategories();
  }

  public filterCategory(content: any) {
    var text = (content ? content.filterValue : '');
    if (text.length >= 3) {
      this.retrieveCategories(text);
    }
  }
 
  public retrieveCategories(querytext?) {
    let queryObj = (querytext ? {'querytext': querytext}: undefined);
    this.stockService.getCategories(queryObj).subscribe((response: any) => {
      this.categories = response.map((item) => {
        item.verbose_name = `${item.id} - ${item.name}`;
        return item;
      });
    });
  }

  public createStock() {
    if (this.form.categories) {
      delete this.form.categories.verbose_name
    }
    this.stockService.createStock(this.form).subscribe(
      (data: any) => {
        this.router.navigate(['/home/list']);
      },
      (error: any) => {
        this.stockError = error.error;
        console.log(this.stockError);
      }
    );
  }
}
