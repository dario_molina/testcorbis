import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
	name: 'filter'
})

export class FilterPipe implements PipeTransform {
	
	public transform(value: any, args: any): any {
		const result = [];
		if (value) {
			args = args.toLowerCase();
			for (const elem of value) {
				let f_name = elem.name.toLowerCase().indexOf(args) > -1;
				let f_code = elem.code.toLowerCase().indexOf(args) > -1;
				let f_price = elem.price.toString().indexOf(args) > -1;
				let f_quantity = elem.quantity.toString().indexOf(args) > -1;
				let f_categories = elem.categories.name.toLowerCase().indexOf(args) > -1;
				if ( f_name || f_code || f_price || f_quantity || f_categories ) {
					result.push(elem);
				};
			};
			return result;
		}
		else {
			return value;
		}
	};

}