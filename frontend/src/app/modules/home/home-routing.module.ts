import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SidenavComponent } from './sidenav/sidenav.component';
import { CreateStockComponent } from '@app/modules/home/stock/create-stock/create-stock.component';
import { ListStockComponent } from '@app/modules/home/stock/list-stock/list-stock.component';
import { DetailStockComponent } from '@app/modules/home/stock/detail-stock/detail-stock.component';
import { UpdateStockComponent } from '@app/modules/home/stock/update-stock/update-stock.component';

const routes: Routes = [
  { path: '', component: SidenavComponent, children: [
      { path: 'create', component: CreateStockComponent },
      { path: 'list', component: ListStockComponent },
      { path: 'detail/:productId', component: DetailStockComponent },
      { path: 'update/:productId', component: UpdateStockComponent },
    ]
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }