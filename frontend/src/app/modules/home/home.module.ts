import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { SidenavComponent } from '@app/modules/home/sidenav/sidenav.component';
import { HomeRoutingModule } from '@app/modules/home/home-routing.module';
import { CreateStockComponent } from '@app/modules/home/stock/create-stock/create-stock.component';
import { ListStockComponent } from '@app/modules/home/stock/list-stock/list-stock.component';
import { StockService } from '@app/services/stock/stock.service';
import { DetailStockComponent } from '@app/modules/home/stock/detail-stock/detail-stock.component';
import { UpdateStockComponent } from '@app/modules/home/stock/update-stock/update-stock.component';
import { FilterPipe } from '@app/modules/home/stock/pipes/filter.pipe';

@NgModule({
  imports: [
    NgSelectModule,
    CommonModule,
    FormsModule,
    HomeRoutingModule,
  ],
  declarations: [
    SidenavComponent,
    CreateStockComponent,
    ListStockComponent,
    DetailStockComponent,
    UpdateStockComponent,
    FilterPipe,
  ],
  providers: [
    StockService,
  ]
})

export class HomeModule { }