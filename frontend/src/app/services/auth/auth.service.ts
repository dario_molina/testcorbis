import { Injectable } from '@angular/core';
import { URL_BACKEND } from '@app/constants';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthService {

  constructor(private http: HttpClient) { }

  public login(params: any) {
    let url = URL_BACKEND + 'api/token/auth/';
    return this.http.post(url, params);
  }

  public getAuthToken() {
    return localStorage.getItem('auth_token');
  }

}