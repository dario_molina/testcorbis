import { Injectable } from '@angular/core';
import { URL_BACKEND } from '@app/constants';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class StockService {

  constructor(private http: HttpClient) { }

  public createStock(params: any) {
    let url = URL_BACKEND + 'api/stock/';
    return this.http.post(url, params);
  }

  public getCategories(queryObj: any) {
    let url = URL_BACKEND + 'api/category/';
    let params = queryObj ? queryObj : {};
    return this.http.get(url, {'params': params});
  }

  public getAllProducts(queryObj?:any) {
    let url = URL_BACKEND + 'api/stock/';
    let params = queryObj ? queryObj : {};
    return this.http.get(url, {'params': params});
  }

  public deleteProduct(id) {
    let url = URL_BACKEND + 'api/stock/' + id;
    return this.http.delete(url);
  }

  public getProduct(id: Number) {
    let url = URL_BACKEND + 'api/stock/' + id;
    return this.http.get(url);
  }

  public updateProduct(id: Number, params: any) {
    let url = URL_BACKEND + 'api/stock/' + id + '/';
    return this.http.patch(url, params);
  }

}