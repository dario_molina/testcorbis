Tecnologías usadas:

* Python 3.67
* Django 2.0
* Django Rest Framework 3.9.1
* Angular 7.3.8
* Postgres 11.2
* Bootstrap 4.3.1

# Instalación de Docker

Para esta aplicación se usan imagenes de python, node y postgres, mas precisamente:

    * python:3.6.7-alpine
    * node:10.15.3-alpine
    * postgres:11.2-alpine


#### Actualizar los repos

    $ sudo apt-get update

#### Instalar unidades

    $ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common -y

#### Agregar el GPG key (para validar la integridad de los archivos)

    $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

#### Agregar el repo

```sh
    $ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

#### Actualizar de nuevo para actualizar el repo
    $ sudo apt-get update

#### Instalar docker
    $ sudo apt-get install docker-ce

#### Iniciarlo con el sistema
    $ sudo systemctl enable docker

#### Agregar usuario al grupo docker
    $ whoami // Saber el nombre de tu usuario
    $ sudo usermod -aG docker nombre_de_salida_whoami

#### Correr el build para generar la imagen
    $ sudo docker-compose build

#### Levantar los servicios de docker (python, node, postgres)
    $ sudo docker-compose up

#### Correr las migraciones

Esperar a que se levanten los servicios de docker, y luego ingresar al contenedor de python en otra shell, con el siguiente comando

    $ docker exec -it py sh

Nos situamos en la carpeta en donde se encuentra el archivo manage.py y correr las migraciones

    $ cd testcorbis/
    $ python manage.py migrate

Creamos un super usuario

    $ python manage.py createsuperuser

Salimos del contenedor de python

    $ exit

#### Ingresar al admin de django

Se debe ingresar al admin de django para poder agregar usuarios y categorías.
En un navegador web agregar e ingresar a:

    $ localhost:8000/admin (Para el ingreso al admin de django usamos el super usuario creado anteriormente)

#### Ingresar a la aplicacion

En un Navegador web agregar:

    $ localhost:4200 (Para el ingreso a la aplicación)
